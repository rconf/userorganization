## General

There are no clocks in the conference rooms, please be prepared to use your own watch or phone.

There are at least 2 wireless microphones per room (4 in the plenary room). So the chair can either share the fixed mic with the speaker or use one of the wireless mics, as you see fit.

There will be at least one student per room to help with any issues and they will circulate with the wireless mic during question time (if applicable, see below).
 
There will be an AV support person in every room to help with the audio and video (not the computer!)

Chairs should

 *	remind the speakers that they should not move away from the computer, because a fixed camera will record them.
 *	remind the speakers that they should speak in the microphone, because the sound will also be recorded.
 *	ask whether the speakers agree to share their slides; make sure the student helper has this information to email user2019@r-project.org the name of the speakers who do not want their slides to be put online.

The slides must not be deleted from the computer because they will be collected and made available after the conference.

## Code of conduct (CoC)

Please familiarise yourself with the code of conduct: http://www.user2019.fr/coc/.

Any code of conduct violation that happens in your session should be reported to the response team, even if you addressed it during the session. This helps to monitor the effectiveness of the code of conduct enforcement and allows the team to decide if any further action is necessary, e.g. making a general announcement to remind people of the code of conduct or to support any action you took.

Feel free, but not obliged, to interrupt the speaker to give a warning in the event of a code of conduct violation. If relevant, the screen may be blanked and offending slides skipped. Interrupting may not always be appropriate or feasible - the violation may be a minor gaffe, you may not respond quickly enough or be at some distance from the speaker, or the presentation may be a lightning talk. In such cases, you may apologize to the audience at the end of the talk/session that a violation occurred and state that this incident will be followed up after the session.

The CoC team are identified with a blue arm band during the conference and may be contacted at coc@user2019.fr.

## Keynotes

The local organizers may wish to give announcements at the start of the session; they will also be responsible for the opening/closing remarks.

The chair is also responsible for chairing the sponsor talk(s) preceding the keynote, see (http://www.user2019.fr/program_overview/). We have the current information regarding speakers representing sponsors:

 * 15 min, Deloitte, Friday morning,  María Paula Caldas, Ignacio Inoa.
 * 15 min, R Studio, Thursday morning	
 * 15 min, R Consortium, Wednesday morning, Joseph (Joe) Rickert
 * 10 min, ThinkR, Thursday afternoon, Diane Beldame
 * 10 min, Airbus, Wednesday afternoon, Christophe Regouby 
 * 5 min, Safran, Thursday afternoon, Sébastien Da Veiga
 * 5 min, OpenAnalytics, Wednesday afternoon, Laure Cougnaud, Machteld Varewyck

Sponsors giving 5 minute talks have been told there will be no time for questions, sponsors giving 10/15 minute talks may leave time for one or two questions if they wish. Ask the speakers what they prefer.

Before a keynote talk, the chair must remind the audience that the questions are asked only via sli.do
 * Remind the audience how to connect to sli.do: go to "slido.com" and enter the event code "useR2019" (case insensitive)
 * After the presentation, switch to slido using "Alt+Tab", and ask the most popular question, which is highlighted.
 * You can swap between the presentation and slido using "Alt+Tab".
 * The local organisers in charge of slido will remove the question after it is answered. Switch to the next question if time permits.
 * Use the microphone to ask questions (so that they are recorded).
 * If there is no question left on sli.do, or if you feel that sli.do is not working well for some reason, you can switch back to the standard QA: asking for questions in the audience.

The chair should prepare a short (< 1 min) introduction to the speaker, this might draw from their bio (http://www.user2019.fr/program/) or otherwise highlight their contribution to R/data science community.

Allow 50 minutes for the talk and 10 minutes for questions. Signs will be provided to let the speaker know that there are X minutes left, or that the time is up.
  => 10 minutes, 5 minutes and STOP


## Regular talks

Slides have to be uploaded on the computer of the room at the latest during the break before the session. Chairs must be present in the room 10 min before the session in order to make sure that the slides are uploaded and/or help uploading the last slides. A folder will have been created with the name of the day and the name of the session as given at: https://user2019.r-project.org/program_overview/ (example: "Monday_Applications 2").

The chair should briefly introduce each talk (title and speaker).

Chairs must be strict on timing: presentations are 18 minutes long *including* 3 minutes for questions. Signs will be provided to let the speaker know that there are X minutes left, or that the time is up.
  => 5, 2 and STOP

The chair should handle the question time, i.e. ask for questions, select who should ask their question (favouring under-represented groups if possible) and bring the questions to a close in time for the next talk.

## Lightning talks

A pdf containing the slides of all speakers of the session will be uploaded in advance in the session room. The student helper will begin the slideshow at the start of the session, when you are ready. They can also help to pause the slideshow if necessary, but it is intended that the full session should run in one slideshow.

Each slide of the pdf file will be displayed for 20 seconds. A 20 second slide with the name of the last and next speakers will be included between each presentation to switch presenters.

Chairs must make sure that the next speakers are ready, so that they can take the microphone before the end of the 20-second transition slide.

Each talk is 5 minute long. No questions are allowed after the 5 minute slot (but questions are allowed if the speaker dedicated the last few slide slots to the QA).

