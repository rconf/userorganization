# Live captioning

For live captioning to be most effective, captioners will need briefing with relevant details, see [planning for CART](https://www.hearingloss.org/hearing-help/technology/cartcaptioning/) for more information.

Most captioners can offer captioning on-site, off-site (captioner in different location from conference), or on-line (for virtual events).

Live captioning can vary in cost from USD 125 to USD 600 per hour, see [What impacts the price of captions?](https://blog.ai-media.tv/blog/what-impacts-price-of-captions#:~:text=In%20short%2C%20the%20price%20of,hour%20to%20%24300%20per%20hour). On-site quotes will include travel and subsistence costs for captioners. All quotes will include some time for testing and preparation, so the headline figures below are for rough budgeting.

## Captioners where we have quotes

See other files in this directory for details

 - ACS: on-site (2020: USD 2200/day, 5 parallel sessions) or off-site (2020: USD 125/hour)
 - White Coat captioning: on-site (2020: USD 35000), offsite (2020: USD 27000) and virtual (2020: ~USD 600/hour).
 
In 2020 they used White Coat captioning.
 
## Others to consider

 - AI media, recommended by University of Melbourne
https://www.ai-media.tv/products/live-content/zoom-meeting-live-captions/
 - Captioning star https://www.captioningstar.com/remote-live-captioning/