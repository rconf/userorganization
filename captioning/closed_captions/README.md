# Closed captions

## Captioning in the video language

Closed captions can be added to recorded video by a captioning service. Choose a service that uses professional transcription or editing for better accuracy. Some services (e.g. [Rev](https://www.rev.com/rev.com)) will add captions based on a transcript, if you have it.

Another option if you have a transcript available, is to auto-sync it with the video on YouTube see [help on Auto-sync](https://support.google.com/youtube/answer/2734796#create). The quality may not be as good as providing a transcript to a captioning service (untested, data needed!).

Some services only provide captioning in one or a restricted set of languages. The captioners we have prices for are cheapest for captioning in English.

Rev.com was used in 2020.

## Subtitling English videos in other languages

Rev.com provides this service at an affordable rate, e.g. Spanish is USD 3/min, with English captions free. So could be worth captioning in multiple languages at the same time if funding available.

## Subtitling non-English videos in English

GoTranscript provides this service at a rate that is cheaper than translating the non-English transcript with a translation service (e.g. USD 7.5/min for English subtitles on Spanish video, vs say https://gengo.com/pricing-languages/ that charges $0.12/word ~ USD 14-18/minute at 120-150 words/min).