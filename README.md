# useR! Organization

Information on useR! organization that can be made public - files should not contain information such as personal email addresses that are not public information.

Some sensitive information, e.g. past balance sheets, sponsor contact details/amounts, 
is available in a separate repository (useRadmin) that will be shared with 
specific members of the organizing team, for use on a need-to-know basis.