# Maintaining Sponsor Contacts

It is helpful to share sponsor contacts from one year to the next, but it is 
important to do so in a way that respects contacts' rights and is compliant with 
laws such as the European GDPR regulations.

## Sponsor contacts from past conferences

Contacts from past useR! conferences are stored in the useRadmin repo. Ask a 
lead organizer (e.g. chair of organizing committee, global co-ordinator), if 
you need this information.

General information on past sponsors (sponsors and their sponsorship level) is 
shared here.

## Sponsor contacts for the current conference

Local organizers should maintain their own contact lists for managing contracts 
with sponsors. You should take care to protect personal data by following 
good data security practices. In particular:
 - store the contacts outside your main conference repo, in a way that only the 
 people that need the contact details have access.
  - do not share contact details (email addresses, phone numbers) with 
  third parties (e.g. another conference) without the contact's consent.
  
## Passing on sponsor contacts

Please help future organizers by passing on sponsor contact details where you 
have consent. This information can be stored in the useRadmin repo by a lead 
organizer.

# Other

The useRadmin repo has an example sponsorship contract.