## General E-Mail setup


Backend:
we worked with GSuite and the @user2021.ch domain. these accounts can be configured to send outgoing mail from @r-project adresses. (Instructions will follow)

Frontend: 
We set up the following addresses with Martin Maechler:

user2021@r-project.org
user2021-sponsoring@r-project.org
user2021-program@r-project.org
user2021-coc@r-project.org
user2021-accessibility@r-project.org