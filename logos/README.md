# Guidelines for useR! Logo

The official logo is the useR! with halo, in either all blue or blue + gray.
This logo may be used as the default logo for a useR! conference.

## Customisation

The useR! logo may be customised for a given useR! conference, but should 
remain recognisable.

Please follow the following guidelines:

 - the color of "use" and "!" can be changed
 - the style of the exclamation mark can be changed, but it should
 always be included in the official logo (it can be dropped e.g. in the 
 image used for social media profiles/website favicon where space is very
 limited)
 - the font of "use" and "R" should stay the same
 - any image can be used instead of the usual grey halo (subject to
 general decency!)
 - there should be a version that is close to square aspect ratio
 
## Customisation package

A package is available to help with basic customisation of the logo: https://github.com/lockedata/userlogo