library(rtweet)

user2020stl <- get_timeline("useR2020stl", n=1000)
user2020muc <- get_timeline("useR2020muc", n=1000)
UseR2019_Conf <- get_timeline("UseR2019_Conf", n=1000)
useR2018_conf <- get_timeline("useR2018_conf", n=1000)
user2015aalborg <- get_timeline("user2015aalborg", n=1000)
user2014_ucla <- get_timeline("user2014_ucla", n=1000)

conferences <- rbind(user2020stl,user2020muc,UseR2019_Conf, useR2018_conf, user2015aalborg,
                     user2014_ucla)
                     
conferences2 <- conferences[conferences$is_retweet==FALSE,]
saveRDS(conferences2, "useR_conference_tweets.rds")
conf3 <- conferences2[ , colSums(is.na(conferences2)) == 0]
conf3 <- subset(conf3, select = -c(geo_coords, coords_coords, bbox_coords))
write.csv(conf3, "useR_conference_tweets.csv", fileEncoding = "utf8")