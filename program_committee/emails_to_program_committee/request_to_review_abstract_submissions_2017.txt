Dear All,

We allocated all abstracts for different topics. Each topic has a review team of two people. You can see the list of topics and review team per topic below.

Each of us will have 67-69 abstracts to review. 

We have 408 contributed abstracts to review and we plan  150 talks, 60 lightning and 100 posters so we should expect to reject around 1 in 4 abstracts.

You can see the abstracts using the link and details that Tobias sent you before.

Please enter your review in the description field of the online tracker (one reviewer beneath the other). In your review please include the following:

 A. Short review. A few words is enough for a clear case, otherwise 1 or 2 sentences justifying your recommendation

 B. Your recommendation, one of

  1. accept as talk

  2. accept as lightning

  3. accept as poster

  4. borderline

  5. reject

For talks, suggest one or two sessions the talk would suit. Use the same names as in the topics field if suitable (e.g. Bioinformatics, High Performance Computing) or suggest your own (e.g. Parallel Computing). Also if you notice good pairings within other the abstracts you review, please note this.

Please finish to review the abstracts that were allocated for you by 25/04/2017 so we will be able to send notification for the speakers/presenters by May 01, 2017.

I will contact you with more deatils later on today.

Thank you very much for your support, best regards, ziv.
