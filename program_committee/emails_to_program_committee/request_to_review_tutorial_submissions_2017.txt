Dear All,

The results of our tutorial invitations were as follows

8 accepted:
Dirk Eddelbuettel: Rcpp
Stephanie Kovalchik: Sport analytics
Karline Soetaert: Environmental modelling
Hana Ševčíková: Parallel programming for beginners
Martyn Plummer: JAGS
Gabor Csardi: R-Hub
Charlotte Wickham: purrr
Edzer Pebesma: sf

3 declined:
...

We received 23 submissions in response to our open call. So we now need
to select 8 of these to make up the 16 tutorials we have space for.
Attached is a pdf of the collated submissions and a csv summary. Please
vote (with a simple Y/N in the csv) for the 8 tutorials you think should
be selected.

**Please respond by Wednesday 1 February**

Many thanks,

Heather