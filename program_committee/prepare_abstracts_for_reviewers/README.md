This is example code for compiling submitted abstracts into a combined HTML.

The files are as follows:

 - compile_abstracts_in_HTML.R is the master script

 - abstracts.md.rsp is the template

 - tutorials_submissions_utf8.csv is the source data encoded in UTF-8 (encoding is very important, be careful if open in spreadsheet programs!)

 - tutorial_abstracts_group1.html is the example output