# setwd to file location where abstract saved as CSV in UTF-8 encoding

library(R.rsp)

# set encoding to UTF-8
opts <- options(encoding = "UTF-8")

# compile abstracts group by group, here group 1 for illustration
group <- 1
file_prefix <- "tutorial_abstracts_group"

# DO NOT post-process (convert md to HTML) as need to work round bug on Windows
rfile("abstracts.md.rsp", 
      args = list(
        group = group,
        title = "useR! 2019: Submitted tutorials",
        author = "Pierre Neuvial, for the useR! 2019 program committee",
        conference = "useR! 2019",
        url_prefix = "https://user2019.sciencesconf.org/review/papers?docid=",
        abstracts_csv = "tutorial_submissions_utf8.csv"),
      output = paste0(file_prefix, 1, "md"),
      postprocess = FALSE)

# here's where we work round bug: should use writeLines with useBytes = FALSE
# as text has already been encoded as UTF-8
html_text <- markdownToHTML(paste0(file_prefix, 1, ".md"))
writeLines(html_text, "tutorial_abstracts_group1.html")

# optional tidy up
file.remove(paste0(file_prefix, 1, ".md"))

# return to previous options
options(opts)



