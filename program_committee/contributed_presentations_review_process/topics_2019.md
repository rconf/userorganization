# Topics

Submitters were asked to select one topic, but the form allowed multiple 
selection. These are the numbers after assigning to most relevant topic (in 
some cases changing the selection made during submission).

biostatistics/epidemiology	52
spatial/time series	47
community/education	47
visualisation	46
applications	44
web app	42
models	39
bioinformatics	35
programming	33
data mining	30
statistics in social sciences	29
big/high dimensional data	28
multivariate analysis	27
other	24
economics/finance/insurance	23
reproducibility	22
interfaces	21
algorithms	21
databases/data management 20
performance	13

Note "interfaces" has changed over the years: used to be more about interfaces 
with other languages/software, now includes dev/ops (e.g. plumber, 
cloud computing, docker, security, CI/CD) so might require different expertise 
from reviewers.