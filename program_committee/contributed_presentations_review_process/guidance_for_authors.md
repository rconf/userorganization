# useR! abstract submission: guidance for authors

useR! is a scientific meeting for all users of R, bringing together participants 
from academia, business, government and other sectors.

There is a selective review process for all contributed presentation formats.

## Presentation formats

Authors can select their preferred format from the following:

- *Regular talk* (18 minutes, with 3 minutes for questions)
    - For novel, mature, substantial contributions.
    - May incorporate live coding or interactive graphics.
- *Lightning talks* (5 minutes)
    - For fun topics, short topics and case studies.
    - Presentation restricted to automatically advanced PDF slides (15 slides, displayed 20 seconds each).
- *Poster* (A1 poster + 30 second flash presentation)
    - For case studies, works-in-progress, and projects for discussion/feedback.
    - Static poster plus 1 slide summary for flash poster session.

[Full technical/logistical details of sessions/formats can be given elsewhere].
    
The program committee will accept abstracts for presentation in the format they 
consider most appropriate, taking into account the author's preference.

Proposals for contributions in an alternative format, e.g. a discussion 
panel, should be submitted as type *Other*. Typically there is space for at 
most one such event in the schedule.

## What do the program committee look for?

The program committee assess the **contribution** of the abstract, looking for 
abstracts that are

- *Novel*, e.g. presenting a new package, a substantial development of a package, a case study, a new community initiative, new resources for the community.
    - The contribution should be new since last useR!
    - Preference may be given to contributions not yet presented at other (nearby/R) conferences.
    - Novelty will be judged relative to other contributions.
- *Related to R*
    - General data science topics are out of scope.
- *Originating from the presenter* (and/or co-authors)
    - Presentations about other people's packages should contain a new contribution, e.g. an extension, benchmarks, a substantive review.
- *Using sound scientific practices*
    - Reliable methodology may be identified by e.g. cited (peer-reviewed) publications, evidence of testing/evaluation, authors' track record.
    - Good programming practice may be identified by e.g. CRAN/Bioconductor publication, examples, vignettes, tests.
- *In a mature state/in progress* ***at the time of abstract submission***
    - Case studies/applications should be completed or clearly on track to report results at useR!
    - Code/packages/apps must be ready for use or for testing/evaluation.
- *Something that participants can readily apply in their own work*
   - Packages should be on CRAN/Bioconductor, or on GitHub/R-Forge/other public repository with evidence of active development
   - Methods in case studies/apps should be clearly described and/or documented, e.g. in a paper/blog post/vignette

Therefore, to maximise chance of selection

   - Make the contribution to the R community clear.
   - Be explicit about R packages used, or other connections to R.
   - Link to supporting materials, give references
   - Be active prior to and (for on-going projects) during the review period
 
## Rejection/reallocation rates

The following plot shows the reallocation/rejection rates for useR! 2019.

<center>
<img id="noborder" src="reallocation_rejection.svg" width="650">
</center>

## Funding

At least one author of each accepted abstract will be expected to attend useR! and must pay standard  registration fees.

Authors who require an accepted presentation in order for their employer to fund them to attend useR! are advised not to pay for non-refundable flights, accommodation or registration before the notification date.

Scholarship applicants who wish to present should submit an abstract before the abstract submission deadline. If your abstract is accepted but your scholarship application is unsuccessful and you have no other source of funding, you must withdraw your presentation from the program.

## General instructions

Abstracts should be written in English since presentations must be given in English.

[Other details of what is required e.g. word count etc could be given here]
