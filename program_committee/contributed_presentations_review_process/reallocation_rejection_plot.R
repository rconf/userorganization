library(readr)
library(dplyr)
library(ggplot2)
library(janitor)
library(tidyr)
library(viridis)

format <- c("talk", "lightning talk", "poster", "reject")
label <- c("Talk", "Lightning talk", "Poster", "Rejected")

dat <- read_csv(file.path("program_committee", 
                          "contributed_presentations_review_process",
                          "decisions_2019_raw.csv")) %>%
    mutate(preferred = factor(preferred,
                          levels = format,
                          labels = label),
           decision = factor(decision,
                             levels = rev(format),
                             labels = rev(label))) %>%
    group_by(preferred) %>%
    mutate(proportion = count/sum(count))

ggplot(dat, aes(x = preferred, fill = decision, y = proportion)) +
    geom_bar(stat = "identity") +
    scale_fill_viridis(discrete=TRUE, option = "magma") +
    labs(x = NULL, y = "Proportion") +
    scale_y_continuous(expand = c(0,0)) +
    scale_x_discrete(expand = c(0,0)) +
    theme(panel.grid.major = element_blank(),
          panel.grid.minor = element_blank(),
          panel.background = element_blank(),
          legend.title = element_blank(),
          legend.text=element_text(size=14),
          axis.text=element_text(size=14),
          axis.title=element_text(size=14),
          plot.title = element_text(hjust = 0.5))

ggsave("reallocation_rejection.svg", width = 10, height = 5)

tmp <- dat %>%
    arrange(preferred, desc(decision)) %>%
    pivot_wider(names_from = decision, values_from = count, 
                id_cols = -proportion) %>%
    adorn_totals(c("row", "col"))
