# Tutorial review process

## Method

Prior to useR! 2019, the number of abstracts was relatively low, ~30. The program committee were asked to review all abstracts and give a yes/no vote, with a target number of yes votes. The final selection was then made by the program chairs based on the PC votes 

For useR! 2019, the number of abstracts was higher that usual (51). The abstracts were split into two broad groups, with half the PC assigned to each:
 - Group 1: statistical modelling and methods
 - Group 2: everything else
PC members were asked to review all abstracts in their group, but could skip abstracts if they felt unqualified to give an opinion. The reviews used the same system as standard abstract review, so reviewers were asked to score each abstract from 0 to 4:

0 - bottom of the pile  
1 - okay, but not inspiring  
2 - middle of the road  
3 - solid choice  
4 - top notch, really hope this one is picked

with a optional comment. 

For useR! 2014, the organizers used a different method, where two reviewers were assigned to each abstract, giving a binary score and the combined score was used to rank tutorials. This did *not* work well, the program committee felt there was reviewer bias and the ranking did not lead to a balanced program, with some areas over-represented. Since a relatively small number of tutorials must be selected from a large number of diverse tutorials, it is hard to get the balance right if reviewers only look at a small number of abstracts in a narrow field. In the 2014 case, this led to a lot of discussion among the PC, with a large number of PC members effectively reviewing all abstracts anyway.

As in 2019, if the number of submissions is high, reviewing all abstracts may not be reasonable, but splitting into 2 or 3 large groups can avoid the problems highlighted above - the final balancing can be done as usual by the program chairs.

## Considerations for reviewers/chairs

Reviewers are asked to score the abstracts on the basis of
 - the merit of the proposal, including value of tutorial vs instructor giving a talk or participants self-learning from documentation
 - suitability for useR! audience
 - balance of program in terms of topic and level (beginner/intermediate/advanced)
 
Chairs should make the final selection taking into account the reviewers scores and aiming for a good balance of topics and levels, as well as a diverse group of instructors. In 2019, an equal number was selected from each group.

## Abstract format

Prior to useR! 2019, abstracts were fairly long (~1 page A4 PDF) and roughly followed a suggested structure specified by the organizers/PC. Examples (from accepted tutorials) are given in the same directory as this document. E.g. for 2017:

- Title, Author(s), Keywords
- Overiew
- Goals/Aims/Learning objectives
- Tutorial content with detailed outline
- Target audience and prerequisites
- Instructor short biographies
- Justification for inclusion at useR!

The proposal would be added to the website as is (without the justification section), though instructors could modify/update it.

In 2018, abstract submission was by a structured webform, with fields (\* mean required field)

- Email\*
- Name\*
- Affiliation\*
- Tutorial title\*
- Outline (provide a paragraph with topics to be covered and rough timing)\*
- Keywords (give us five)\*
- Equipment (what do you and participants need) \*
- R background of participants\*
    - None
    - Beginner
    - Intermediate
    - Advanced
- Other background needed  
- Target audience\*
- Motivation (why do you think this would be a good tutorial for participants of useR! 2018)\*

After acceptance, instructors were asked to modify their proposal for inclusion on the website via a form with the fields:

- Email\*
- Name\*
- Affiliation\*
- Tutorial title\*
- Target audience\*
- Participant to bring\*
- Overview\*
- Learning objective 1\*
- Learning objective 2
- Learning objective 3
- Session 1 details (1.5 hours before break) *
- Session 2 details (1.5 hours after break) *
- Other information useful/needed for participants to know to help them decide 

In 2019, tutorial submission was free-form, limited to 1200 characters (plus keywords). Although this short form made the abstracts quicker to review, the structure was more variable. Aspects such as learning objectives, tutorial outline and instructor bios were not always present. Accepted abstracts were added to the website and instructors were asked to provide details for new "target audience" and "instructions" sub-sections (where "instructions" specified how participants should prepare).

Possibly the shorter form in 2019 encouraged more submissions - the quality did not suffer and review was quicker. However, the web form approach of 2018 helps to enforce some standardisation/minimum requirements, while ensuring the abstracts keep to a manageable length. Some structure also helps novice contributors to know what to include. Therefore using a web form similar to useR! 2018 is recommended.