## How to set up a new useR! website

These instructions are for setting up a new useR! website based on the Quarto template at https://github.com/satRdays/quarto-satrdays-template.

This will be done by a member of the R Foundation Conference Committee ready to pass on to the useR! organizing team.

### Create a new repository

1. Log on to GitLab with the R-conferences account and navigate to the 
R Foundation Conferences group: https://gitlab.com/rconf.

2. Create a new project with a blank repository for the year the site will be for:
 - Project name: useR [YEAR] website.
 - Project URL (follows from above): https://gitlab.com/rconf/user-[YEAR]-website.git.
 - Visibility: public.
 - Do not set a project deployment target. 
 - Do not initialize with a README.
 - Enable Static Application Security Testing (SAST).

### Use the satRdays template

[instructions written after the fact, need checking]

1. In a terminal with access to git, navigate to a directory where you will clone the useR! website repository.

2. Clone the useR! website repository.

    ```
    git clone https://gitlab.com/rconf/user-[YEAR]-website.git
    ```

3. Use the satRdays template
    
    ```
    cd user-[YEAR]-template
    quarto use template satRdays/quarto-satrdays-template
    ```

    You'll be asked to create a new directory due to the `.gitlab-ci.yml` file created for the SAST workflow. Create a temporary directory, then move the contents back to the top level before committing.

4. Commit and push the changes

    ```
    git add --all
    git commit -m "use satRdays quarto template"
    git push
    ```

    Note, if you get the error

    > remote: HTTP Basic: Access denied

    you may need to run the following command first to be prompted for your username and password:

    ```
    git config --system --unset credential.helper
    ```

    (if using the Windows command prompt you will need to be running as Administrator). Alternatively, you can include your credentials in the call to git push

    ```
    git push --mirror https://{username:password@}gitlab.com/rconf/user-[YEAR]-website.git
    ```
    
### Deploy to Netlify

Create a new site as follows (as in https://quarto.org/docs/publishing/netlify.html#publish-from-git-provider)

1. Login to Netlify via the R-conferences GitLab account.
2. On the Sites tab click "Add new site" > "Import an existing project"
3. Select GitLab as the git provider.
4. Select the repository created in the previous steps (select "R Foundation Conferences" as the account to find repos from)
5. Leave the build command empty.
6. Use the publish directory `_site`.

Update project files to render on Netlify (ref: https://quarto.org/docs/publishing/netlify.html#rendering-on-netlify). No GitLab CI/CD equired.

1. Add `.gitignore`

    ```
    **/.DS_Store
    .Rproj.user
    *.Rproj

    /.quarto/
    /_site/
    ```
2. Add `netlify.toml`

    ```
    [[plugins]]
    package = "@quarto/netlify-plugin-quarto"
    ```
3. Add `package.json`

    ```
    {
      "dependencies": {
        "@quarto/netlify-plugin-quarto": "^0.0.5"
      }
    }
    ```
4. Add README for web maintainers (copy from last year).


(Could leave for organizers) Update details:

1. Update text and links in `_quarto.yml` (see previous year for reference).

Then set up branch deployment:

1. Select the new site on the Sites tab page. 
2. Go to Site configuration > Build & deploy > Continuous Deployment > Branches and deploy contexts, and select Configure. Select "Branch deploys: All".

### Set up R project URL

See instructions in private useRadmin repo: https://gitlab.com/rconf/useRadmin/-/blob/master/website/setting_up_new%20website.md.
